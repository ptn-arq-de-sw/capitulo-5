import urllib, json

def get_information(request):
    title = request.args.get("t")
    api_key = '97e082bd'
    url_base = 'http://www.omdbapi.com/'
    # Se conecta con el servicio de IMDb a través de su API
    params = urllib.parse.urlencode({'t': title, 'apikey': api_key, 'plot': 'full', 'r': 'json'})
    url_omdb = urllib.request.urlopen(url_base + "?%s" % params)
    # Se lee la respuesta de IMDb
    json_omdb = url_omdb.read()
    # Se convierte en un JSON la respuesta recibida
    omdb = json.loads(json_omdb)
    # Se regresa el JSON de la respuesta
    return json.dumps(omdb), 200
