import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class SCPConnector extends PolymerElement {
  static get template() {
    return html`
      <iron-ajax
            method="{{method}}"
            id="connector"
            url="[[url]]"
            content-type="[[content]]"
            on-response="_handleResponse"
            handle-as="json"
            body='[[body]]'>
        </iron-ajax>
    `;
  }
  static get properties() {
    return {
      urlBase: {
        type: String,
        value: 'https://naht-redn-dev.cloud.tyk.io/'
      },
      endPoint: {
        type: String,
        value: ''
      },
      url: {
        type: String,
        value: ''
      },
      body: {
        type: String,
        value: ''
      },
      query: {
        type: String,
        value: ''
      },
      content: {
        type: String,
        value: 'application/json'
      },
      api: {
        type: String,
        value: 'spc/'
      },
      language: {
        type: String
      },
      method: {
        type: String,
        value: ''
      }
    };
  }

  _performQuery(){
    this.$.connector.generateRequest();
  }

  _hasValidBody(){
    return this.body !== undefined && this.body !== '';
  }

  _handleResponse(response){
    let res = response.detail.response;
    if(res !== null && res !== undefined){
      this.dispatchEvent(new CustomEvent('done', {detail: {data: res, query: this.query}}));
    } else {
      this._launchError("Query not contains data");
    }
  }

  _launchError(message){
    this.dispatchEvent(new CustomEvent('error', {detail: {data: message, query: this.query}}));
  }
  
  getMovieInformation(title) {
    this.endPoint = `movie_information?t=${title}`;
    this.url = this.urlBase + this.api + this.endPoint;
    this.query = 'getMovieInformation';
    this.method = 'GET';
    this._performQuery();
  }
  
  getTweets(username) {
    this.endPoint = `get_tweets_by_user?q=${username}`;
    this.url = this.urlBase + this.api + this.endPoint;
    this.query = 'getTweets';
    this.method = 'GET';
    this._performQuery();
  }
  
  performAnalysis(tweets){
    this.endPoint = 'perform_analysis';
    this.url = this.urlBase + this.api + this.endPoint;
    this.query = 'performAnalysis';
    this.method = 'POST';
    this.content = "application/json"
    this.body = tweets;
    this._performQuery();
  }
}

window.customElements.define('scp-connector', SCPConnector);