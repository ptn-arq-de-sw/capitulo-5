import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';

class SCPChip extends PolymerElement {
  
  static get template() {
    return html`
      <style>
        .mdl-chip {
          height: 32px;
          font-family: "Roboto", "Helvetica", "Arial", sans-serif;
          line-height: 32px;
          padding: 0 12px;
          border: 0;
          border-radius: 16px;
          background-color: var(--paper-red-900);
          display: inline-block;
          color: var(--paper-grey-50);
          margin: 2px 0;
          font-size: 0;
          white-space: nowrap;
          margin: 3px 3px;
        }
        
        .mdl-chip__text {
          font-size: 13px;
          vertical-align: middle;
          display: inline-block;
        }
      </style>
      <span class="mdl-chip">
        <span class="mdl-chip__text">[[label]]</span>
      </span>
    `;
  }
  
  static get properties() {
    return {
      label: {
        type: String,
        value: ''
      }
    };
  }
  
}

window.customElements.define('scp-chip', SCPChip);